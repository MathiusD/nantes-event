default_target: launch

clear:
	@rm -rf dist android ios electron

install:
	@test -e src/conf/config.json || cp src/conf/config.sample.json src/conf/config.json
	@npm install

launch: install
	@npm run serve

build: install clear
	@npm run build

update_cap: build
	@npx cap copy

init_android: build
	@npx cap add android

init_ios: build
	@npx cap add ios

init_electron: build
	@npx cap add electron

start_android: init_android
	@npx cap open android

start_ios: init_ios
	@npx cap open ios

start_electron: init_electron
	@npx cap open electron

init_all: init_android init_ios init_electron