/**
 * Representation of Event
 */
class Event {

    /**
     * 
     * @param {String} id 
     * @param {String} placeDistrict 
     * @param {String} city 
     * @param {String} name 
     * @param {Boolean,String} lsf 
     * @param {String} endAt 
     * @param {String} place 
     * @param {String} day 
     * @param {Boolean,String} free 
     * @param {String} priceDetails 
     * @param {String} placeWebsite 
     * @param {String} startAt 
     * @param {String} location 
     * @param {String} type 
     * @param {Boolean,String} booked 
     * @param {String} placePhone 
     * @param {Boolean,String} childReception 
     * @param {String} description 
     * @param {String} transmitter 
     * @param {String} address 
     * @param {String} category 
     * @param {Boolean,String} canceled 
     * @param {String} day 
     * @param {String} scheduleLink 
     * @param {String} postalCode 
     * @param {String} idManif 
     * @param {String} otherInfos 
     * @param {Boolean,String} postponed 
     * @param {String} mediaUrl 
     */
     constructor(id, placeDistrict, city, name, lsf, endAt, place, free, 
        priceDetails, placeWebsite, startAt, location, type, booked, 
        placePhone, childReception, description, transmitter, address, 
        category, canceled, day, scheduleLink, postalCode, idManif, otherInfos,
        postponed, mediaUrl) {
        this.id = id;
        this.placeDistrict = placeDistrict;
        this.city = city;
        this.name = name;
        this.raw_lsf = lsf;
        this.lsf = Event.convertToBoolean(lsf);
        this.endAt = endAt;
        this.place = place;
        this.raw_free = free;
        this.free = Event.convertToBoolean(free);
        this.priceDetails = priceDetails;
        this.placeWebsite = placeWebsite;
        this.startAt = startAt;
        this.location = location;
        this.type = type;
        this.raw_booked = booked;
        this.booked = Event.convertToBoolean(booked);
        this.placePhone = placePhone;
        this.raw_childReception = childReception;
        this.childReception = Event.convertToBoolean(childReception);
        this.description = description;
        this.transmitter = transmitter;
        this.address = address;
        this.category = category;
        this.raw_canceled = canceled;
        this.canceled = Event.convertToBoolean(canceled);
        this.day = day;
        this.scheduleLink = scheduleLink;
        this.postalCode = postalCode;
        this.idManif = idManif;
        this.otherInfos = otherInfos;
        this.raw_postponed = postponed;
        this.postponed = Event.convertToBoolean(postponed);
        this.mediaUrl = mediaUrl;
    }

    /**
     * Method for convert String (or Boolean) to Boolean
     * @param {String,Boolean} entry 
     * @returns Boolean,StringEquivalent
     */
    static convertToBoolean(entry) {
        return typeof entry == "boolean" ? entry :
            entry != null && entry.toLowerCase() == "oui" ? true : false;
    }

    /**
     * Method for get storage used
     * @returns Storage used
     */
    static storage(){
        return sessionStorage;
    }

    /**
     * Method for get storage key used
     * @returns Key used
     */
    static storageKey(){
        return "Events";
    }

    /**
     * Method for parse event from dict
     * @param {Dict} jsonData 
     * @returns Event parsed or null
     */
    static parse(jsonData) {
        return new Event(
            jsonData.id,
            jsonData.placeDistrict,
            jsonData.city,
            jsonData.name,
            jsonData.raw_lsf,
            jsonData.endAt,
            jsonData.place,
            jsonData.raw_free,
            jsonData.priceDetails,
            jsonData.placeWebsite,
            jsonData.startAt,
            jsonData.location,
            jsonData.type,
            jsonData.raw_booked,
            jsonData.placePhone,
            jsonData.raw_childReception,
            jsonData.description,
            jsonData.transmitter,
            jsonData.address,
            jsonData.category,
            jsonData.raw_canceled,
            jsonData.day,
            jsonData.scheduleLink,
            jsonData.postalCode,
            jsonData.idManif,
            jsonData.otherInfos,
            jsonData.raw_postponed,
            jsonData.mediaUrl
        );
    }

    /**
     * Method for get all events
     * @returns eventList store in sessionStorage
     */
    static getAll() {
        var baseData = Event.storage().getItem(Event.storageKey());
        try {
            var usableData = JSON.parse(baseData);
            for (var [key, value] of Object.entries(usableData))
                usableData[key] = Event.parse(value);
            return usableData;
        } catch (error) {
            return null;
        }
    }

    /**
     * Method for store this event in sessionStorage
     */
    store() {
        var events = Event.getAll();
        if (events == null)
            events = {};
        events[this.id] = this;
        Event.storage().setItem(Event.storageKey(), JSON.stringify(events));
    }

    /**
     * Method for store many event in sessionStorage
     * @param {Array<Event>} events 
     */
    static store(events) {
        events.forEach((event) => event.store());
    }

    /**
     * Method for get event by its id
     * @param {String} eventId 
     * @returns Event if found
     */
    static get(eventId) {
        var events = Event.getAll();
        if (events == null)
            return null;
        return events[eventId];
    }

    /**
     * Method for get default mediaUrl used
     * @returns default mediaUrl
     */
    static defaultMediaUrl() {
        return "https://versions.bulma.io/0.7.1/images/placeholders/1280x960.png";
    }

    /**
     * For get mediaUrl with placeholder if not set
     * @returns mediaUrl
     */
    getMediaUrl() {
        return this.mediaUrl != null ? this.mediaUrl : Event.defaultMediaUrl();
    }
}

export default {
    Event:Event
};