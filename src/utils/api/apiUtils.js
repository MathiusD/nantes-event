import event from '../event';
/**
 * Class taking care of the API.
 */
class APIUtils {
    static url1 = "https://data.nantesmetropole.fr/api/records/1.0/search/?dataset=244400404_agenda-evenements-nantes-nantes-metropole&sort=-date&rows=";
    static url2 = "&q=date>="

    /**
     * Fonction to build the Url for the API request.
     * @param {Integer} rows Number of results expected.
     * @param {String} date Date in String format
     * @returns Url request.
     */
    static getAPIUrl(rows, date) {
        if (date == null || typeof date != "string") {
            date = APIUtils.formatDate(new Date())
        }
        if (rows == null || typeof rows != typeof 0 || rows <= 0) {
            rows = 10;
        }
        var request = APIUtils.url1 + rows + APIUtils.url2 + date;
        return request;
    }

    /**
     * Fonction to format the Date to format 'yyyy/mm/dd".
     * @param {Date, String} date Date to format
     * @returns Date in String format.
     */
    static formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('/');
    }
    
    /**
     * Map the data (Array of Objects) to an Array of Events and store the data.
     * @param {Array} data Array of data get from the API.
     * @returns Array of Events to display.
     */
    static dataToEvent(data) {
        var events = [];
        data.forEach(d => {
            events.push(new event.Event(
                d.recordid,
                d.fields.lieu_quartier, 
                d.fields.ville, 
                d.fields.nom,
                d.fields.h_lsf, 
                d.fields.heure_fin,
                d.fields.lieu,
                d.fields.gratuit,
                d.fields.precisions_tarifs,
                d.fields.lieu_siteweb,
                d.fields.heure_debut,
                d.fields.location,
                d.fields.type,
                d.fields.complet,
                d.fields.lieu_tel,
                d.fields.accueil_enfant,
                d.fields.description,
                d.fields.emetteur,
                d.fields.adresse,
                d.fields.rubrique,
                d.fields.annule,
                d.fields.date,
                d.fields.lien_agenda,
                d.fields.code_postal,
                d.fields.id_manif,
                d.fields.info_suppl,
                d.fields.reporte,
                d.fields.media_url));
        });
        event.Event.store(events);
        return events;
    }
}

export default {
    APIUtils: APIUtils
};