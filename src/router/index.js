import { createRouter, createWebHistory } from 'vue-router';

import HomePage from '../components/HomePage.vue';
import EventList from '../components/EventList.vue';
import Event from '../components/Event.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HomePage
  },
  {
    path: '/events',
    name: 'Events',
    component: EventList
  },
  {
    path: '/event/:id',
    props: true,
    name: 'Event',
    component: Event
  }
];

import config from "../conf/config.json";

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL + config.baseUri),
  base: config.baseUri,
  routes
});
	
export default router;